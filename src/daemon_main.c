/*
**   Version: daemon.c/2023.03.23-1
** Copyright: © 1995 Klaus Alexander Seistrup <klaus@seistrup.dk>
**   License: GNU Affero General Public License v3+
**    Source: https://codeberg.org/kas/daemon
*/
#include <errno.h>
#include <getopt.h>
#include <libgen.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#define VERSION "2023.03.24-1"
#define OPTIONS "++hVn"

static void version(char *progname) {
  (void)printf("  Version: daemon/%s v%s\n", progname, VERSION);
  (void)puts("Copyright: © 1995 Klaus Alexander Seiﬆrup <klaus@seistrup.dk>");
  (void)puts("  License: GNU Affero General Public License v3+");
  (void)puts("   Source: https://codeberg.org/kas/daemon");
}

static void help(char *progname, FILE *fptr, int detach) {
  (void)fprintf(fptr, "Usage: %s [OPTIONS] COMMAND [ARGUMENT …]\n\n", progname);
  (void)fputs("positional arguments:\n", fptr);
  (void)fputs("  COMMAND         command to run\n", fptr);
  (void)fputs("  ARGUMENT        optional argument to COMMAND\n\n", fptr);
  (void)fputs("options:\n", fptr);
  (void)fputs("  -h, --help      show this help message and exit\n", fptr);
  (void)fputs("  -V, --version   show version information and exit\n", fptr);
  if (detach != 0) {
    (void)fputs("\ndetach sets umask to 027 before it detaches itself from\n", fptr);
    (void)fputs("the terminal. Current working directory is left unchanged.\n", fptr);
  } else {
    (void)fputs("  -n, --no-chdir  do not change current working directory\n", fptr);
    (void)fputs("\ndaemon changes the current working directory to the root\n", fptr);
    (void)fputs("directory (“/”). Umask is left unchanged.\n", fptr);
  }
  (void)fprintf(fptr, "\nSee also: %s(1), daemon(3), umask(3).\n", detach != 0 ? "daemon" : "detach");
}

/*
** We use the daemon(3) implementation from musl [almost]
** verbatim so that we know what's actually going on.
**
** FIXME: We should probably #ifdef this.
*/
#include "daemon.c"

int main(int argc, char **argv) {
  char *progname = basename(argv[0]);
  int detach = (int)(strcmp(progname, "detach") == 0);
  int nochdir = detach;
  int index = 0, opt = 0;

  /*@-nullassign@*/
  static struct option lopts[] = {
    {"help",     no_argument, (int *)NULL, (int)'h'},
    {"version",  no_argument, (int *)NULL, (int)'V'},
    {"no-chdir", no_argument, (int *)NULL, (int)'n'},
    {0, 0, 0, 0}
  };
  /*@=nullassign@*/

  (void)setlocale(LC_ALL, "");

  opterr = 0;
  while ((opt = getopt_long(argc, argv, OPTIONS, lopts, &index)) != -1) {
    switch (opt) {
    case 'h':
      help(progname, stdout, detach);
      exit(EXIT_SUCCESS);
    case 'V':
      version(progname);
      exit(EXIT_SUCCESS);
    case 'n':
      nochdir = 1;
      break;
    default:
      opt = optind != 0 ? optind - 1 : 1;
      (void)fprintf(stderr, "Unrecognized option: “%s”\n", argv[opt]);
      (void)fprintf(stderr, "Try “%s --help” for more information.\n", progname);
      exit(EXIT_FAILURE);
    }
  }

  argc -= optind;
  argv += optind;

  if (argc < 1) {
    help(progname, stderr, detach);
    exit(EXIT_FAILURE);
  }

  if (daemon(nochdir, 0) == -1) {
    perror("daemon(3)"); /* nobody will listen, but let's do it anyway */
    exit(EXIT_FAILURE);
  }

  if (detach != 0) {
    (void)umask(0027);
  }

  if (execvp(*argv, argv) == -1) {
    perror("execvp(3)"); /* nobody will listen, but let's do it anyway */
    exit(EXIT_FAILURE);
  }

  exit(EXIT_SUCCESS);  /* not reached */
}

/* eof */
