DETACH(1)	"DETACH v2023.01.11"

# NAME

detach - CLI utility for running a command detached from the current terminal

# SYNOPSIS

*detach* [_OPTIONS_] _COMMAND_ [_ARGUMENT_ …]

# DESCRIPTION

*detach* sets its umask to _0027_ before it uses the *daemon*(3) system call
to detach itself from the controlling terminal, and runs _COMMAND_ (possibly
with one or more arguments).

Contrary to *daemon*, *detach* does not change its current working
directory.

# OPTIONS

*-h*, *--help*
	Print a short help text and exit

*-V*, *--version*
	Show version information and exit

# BUGS

Errors occuring after assigning standard IO to _\/dev\/null_ and forking the
process (such as a command not found in _$PATH_) are unlikely to surface to
the controlling terminal.

# DEVELOPMENT

Please see https://codeberg.org/kas/daemon for full source and issues.

# COPYRIGHT

Copyright © 1995 Klaus Alexander Seistrup <klaus@seistrup.dk>.

# LICENSE

GNU Affero General Public License v3+.

# SEE ALSO

*daemon*(1),
*daemon*(3),
*umask*(3),
*fork*(2).
