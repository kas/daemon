DAEMON(1)	"DAEMON v2023.01.11"

# NAME

daemon - CLI utility for running a command detached from the current terminal

# SYNOPSIS

*daemon* [_OPTIONS_] _COMMAND_ [_ARGUMENT_ …]

# DESCRIPTION

*daemon* changes its current working directory to _\/_ before it uses the
*daemon*(3) system call to detach itself from the controlling terminal, and
runs _COMMAND_ (possibly with one or more arguments).

Contrary to *detach*, *daemon* does not change its umask.

# OPTIONS

*-h*, *--help*
	Print a short help text and exit

*-V*, *--version*
	Show version information and exit

*-n*, *--no-chdir*
	Leave current working directory unchanged

# BUGS

Errors occuring after assigning standard IO to _\/dev\/null_ and forking the
process (such as a command not found in _$PATH_) are unlikely to surface to
the controlling terminal.

# DEVELOPMENT

Please see https://codeberg.org/kas/daemon for full source and issues.

# COPYRIGHT

Copyright © 1995 Klaus Alexander Seistrup <klaus@seistrup.dk>.

# LICENSE

GNU Affero General Public License v3+.

# SEE ALSO

*detach*(1),
*daemon*(3),
*umask*(3),
*fork*(2).
