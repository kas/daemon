#define _GNU_SOURCE

#include <fcntl.h>
#include <unistd.h>

int daemon(int nochdir, int noclose) {
  if ((nochdir == 0) && (chdir("/") != 0))
    return -1;
  if (noclose == 0) {
    int fd, failed = 0;
    if ((fd = open("/dev/null", O_RDWR)) < 0)
      return -1;
    if (dup2(fd, 0) < 0 || dup2(fd, 1) < 0 || dup2(fd, 2) < 0)
      failed++;
    if (fd > 2)
      (void)close(fd);
    if (failed != 0)
      return -1;
  }

  switch (fork()) {
  case 0:
    break;
  case -1:
    return -1;
  default:
    _exit(0);
  }

  if (setsid() < 0)
    return -1;

  switch (fork()) {
  case 0:
    break;
  case -1:
    return -1;
  default:
    _exit(0);
  }

  return 0;
}
