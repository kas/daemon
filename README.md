# daemon & detach

Ultra-minimalistic CLI wrappers around the `daemon(3)` system call for
running a command detached from the current terminal.

Usage: `daemon|detach COMMAND [ARGUMENT ...]`

### `daemon`

Will `cd /` and detach itself from the terminal before running COMMAND
(possibly with arguments).

### `detach`

Will stay in the current directory, set umask to 0027, and then detach
itself from the terminal before running COMMAND (possibly with arguments).

## History

This started as the command `ASyncRun` for AmigaOS back in the '90es.

Nowadays it is literally just a wrapper around `daemon(3)` with no frills
added, except for `detach` that sets umask to 0027 before exec'ing into the
command.
